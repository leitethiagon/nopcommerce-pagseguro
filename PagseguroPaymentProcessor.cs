using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Net;
using System.Text;
using System.Web;
using System.Web.Routing;
using Nop.Core;
using Nop.Core.Domain.Directory;
using Nop.Core.Domain.Orders;
using Nop.Core.Domain.Payments;
using Nop.Core.Domain.Shipping;
using Nop.Core.Plugins;
using Nop.Plugin.Payments.Pagseguro.Controllers;
using Nop.Services.Configuration;
using Nop.Services.Directory;
using Nop.Services.Localization;
using Nop.Services.Orders;
using Nop.Services.Payments;
using Nop.Services.Tax;
using Uol.PagSeguro;

namespace Nop.Plugin.Payments.Pagseguro
{
    /// <summary>
    /// PayPalStandard payment processor
    /// </summary>
    public class PagseguroPaymentProcessor : BasePlugin, IPaymentMethod
    {
        #region Fields

        private readonly PagseguroPaymentSettings _pagseguroPaymentSettings;
        private readonly ISettingService _settingService;
        private readonly ICurrencyService _currencyService;
        private readonly CurrencySettings _currencySettings;
        private readonly IWebHelper _webHelper;
        private readonly ICheckoutAttributeParser _checkoutAttributeParser;
        private readonly ITaxService _taxService;
        private readonly HttpContextBase _httpContext;
        #endregion

        #region Ctor

        public PagseguroPaymentProcessor(PagseguroPaymentSettings pagseguroPaymentSettings,
            ISettingService settingService, IWebHelper webHelper,
            HttpContextBase httpContext)
        {
            this._pagseguroPaymentSettings = pagseguroPaymentSettings;
            this._settingService = settingService;
            this._webHelper = webHelper;
            this._httpContext = httpContext;
        }

        #endregion

        #region Methods

        /// <summary>
        /// Process a payment
        /// </summary>
        /// <param name="processPaymentRequest">Payment info required for an order processing</param>
        /// <returns>Process payment result</returns>
        public ProcessPaymentResult ProcessPayment(ProcessPaymentRequest processPaymentRequest)
        {
            var result = new ProcessPaymentResult();
            result.NewPaymentStatus = PaymentStatus.Pending;
            return result;
        }

        /// <summary>
        /// Post process payment (used by payment gateways that require redirecting to a third-party URL)
        /// </summary>
        /// <param name="postProcessPaymentRequest">Payment info required for an order processing</param>
        public void PostProcessPayment(PostProcessPaymentRequest postProcessPaymentRequest)
        {
            // creates a payment request with pagseguro
            PaymentRequest paymentRequest = new PaymentRequest();
            
            if (_pagseguroPaymentSettings.PassProductNamesAndTotals == false)
            {
                //pass order total
                var orderTotal = Math.Round(postProcessPaymentRequest.Order.OrderTotal, 2);

                Item productItem = new Item(postProcessPaymentRequest.Order.Id.ToString(),
                    string.Format("Pedido # {0}", postProcessPaymentRequest.Order.Id), 1, orderTotal);
                
                paymentRequest.Items.Add(productItem);
            }

            //decimal totalExtraShipping = 0;
            var cartItems = postProcessPaymentRequest.Order.OrderProductVariants;
            foreach (var item in cartItems)
            {
                var unitPriceInclTaxRounded = Math.Round(item.UnitPriceInclTax, 2);

                Item productItem = new Item(
                    item.ProductVariantId.ToString(),
                    item.ProductVariant.FullProductName,
                    item.Quantity,
                    unitPriceInclTaxRounded);

                if (item.ProductVariant.IsFreeShipping == false)
                {
                    productItem.ShippingCost = Math.Round(item.ProductVariant.AdditionalShippingCharge, 2);
                    productItem.Weight = Convert.ToInt64(item.ProductVariant.Weight);
                }
                else
                {
                    productItem.ShippingCost = 0;
                    productItem.Weight = 0;
                }

                //totalExtraShipping += item.ProductVariant.AdditionalShippingCharge;

                paymentRequest.Items.Add(productItem);
            }

            //shipping
            paymentRequest.Shipping = new Shipping();
            switch (postProcessPaymentRequest.Order.ShippingMethod)
            {
                case "PAC":
                    paymentRequest.Shipping.ShippingType = ShippingType.Pac;
                    break;
                case "SEDEX":
                    paymentRequest.Shipping.ShippingType = ShippingType.Sedex;
                    break;
                default:
                    paymentRequest.Shipping.ShippingType = ShippingType.NotSpecified;
                    break;
            }

            //// manually include shipping item if any
            //if (totalExtraShipping <= 0 && postProcessPaymentRequest.Order.OrderShippingExclTax > 0)
            //{
            //    Item shippingItem = new Item("FR", "Frete", 1, postProcessPaymentRequest.Order.OrderShippingInclTax);
            //    paymentRequest.Items.Add(shippingItem);
            //}

            if (postProcessPaymentRequest.Order.ShippingAddress == null)
            {
                throw new NopException("Endereco de entrega � obrigat�rio");
            }

            /// shippind address
            Address shippingAddress = new Address();
            shippingAddress.City = postProcessPaymentRequest.Order.ShippingAddress.City;
            shippingAddress.PostalCode = postProcessPaymentRequest.Order.ShippingAddress.ZipPostalCode;
            shippingAddress.Street = postProcessPaymentRequest.Order.ShippingAddress.Address1;
            shippingAddress.Complement = postProcessPaymentRequest.Order.ShippingAddress.Address2;

            if (postProcessPaymentRequest.Order.ShippingAddress.StateProvince != null)
                shippingAddress.State = postProcessPaymentRequest.Order.ShippingAddress.StateProvince.Abbreviation;
            else
                shippingAddress.State = "";
            if (postProcessPaymentRequest.Order.ShippingAddress.Country != null)
                shippingAddress.Country = postProcessPaymentRequest.Order.ShippingAddress.Country.TwoLetterIsoCode;
            else
                shippingAddress.Country = "BR";
            
            paymentRequest.Shipping.Address = shippingAddress;

            //billing information
            if (postProcessPaymentRequest.Order.BillingAddress == null)
            {
                throw new NopException("Endere�o de cobran�a � obrigat�rio");
            }

            string name = string.Format("{0} {1}",
                    postProcessPaymentRequest.Order.BillingAddress.FirstName,
                    postProcessPaymentRequest.Order.BillingAddress.LastName);
            string email = postProcessPaymentRequest.Order.Customer.Email;

            if (postProcessPaymentRequest.Order.BillingAddress.PhoneNumber.Length < 10)
                throw new NopException("N�mero de telefone inv�lido");

            string areaCode = postProcessPaymentRequest.Order.BillingAddress.PhoneNumber.Substring(0, 2);
            string telephone = postProcessPaymentRequest.Order.BillingAddress.PhoneNumber.Substring(2, postProcessPaymentRequest.Order.BillingAddress.PhoneNumber.Length - 2);

            paymentRequest.Sender = new Sender(name, email, new Phone(areaCode, telephone));
                        paymentRequest.Currency = Uol.PagSeguro.Currency.Brl;
            paymentRequest.Reference = postProcessPaymentRequest.Order.OrderGuid.ToString();
            paymentRequest.RedirectUri = new Uri(GetReturnUrl());

            AccountCredentials credentials = new AccountCredentials(
                _pagseguroPaymentSettings.Email,
                _pagseguroPaymentSettings.Token
            );
            
            Uri url;
            try
            {
                url = Uol.PagSeguro.PaymentService.Register(credentials, paymentRequest);
            }
            catch (PagSeguroServiceException ex)
            {
                if (ex.StatusCode == HttpStatusCode.Unauthorized)
                {
                    throw new NopException("Acesso n�o autorizado.\n");
                }
                string text = "Erros: ";
                foreach (PagSeguroServiceError current in ex.Errors)
                {
                    text += current.ToStr() + "\n";
                }

                throw new NopException(text);
            }
            catch (Exception)
            {
                throw new NopException("Ocorreu um erro inesperado, tente novamente mais tarde.\n");
            }

            if (url == null)
		    {
			    throw new NopException("Ocorreu um erro ao criar uma transacao com o pagseguro.\n");
		    }

            HttpContext.Current.Response.Redirect(url.ToString());
        }

        private string GetReturnUrl()
        {
            if (_pagseguroPaymentSettings.WebSiteUrl.EndsWith("/"))
                return  _pagseguroPaymentSettings.WebSiteUrl + "checkout/completed";
            
            return _pagseguroPaymentSettings.WebSiteUrl + "/checkout/completed";
        }

        /// <summary>
        /// Gets additional handling fee
        /// </summary>
        /// <returns>Additional handling fee</returns>
        public decimal GetAdditionalHandlingFee()
        {
            return _pagseguroPaymentSettings.AdditionalFee;
        }

        /// <summary>
        /// Captures payment
        /// </summary>
        /// <param name="capturePaymentRequest">Capture payment request</param>
        /// <returns>Capture payment result</returns>
        public CapturePaymentResult Capture(CapturePaymentRequest capturePaymentRequest)
        {
            var result = new CapturePaymentResult();
            result.AddError("Capture method not supported");
            return result;
        }

        /// <summary>
        /// Refunds a payment
        /// </summary>
        /// <param name="refundPaymentRequest">Request</param>
        /// <returns>Result</returns>
        public RefundPaymentResult Refund(RefundPaymentRequest refundPaymentRequest)
        {
            var result = new RefundPaymentResult();
            result.AddError("Refund method not supported");
            return result;
        }

        /// <summary>
        /// Voids a payment
        /// </summary>
        /// <param name="voidPaymentRequest">Request</param>
        /// <returns>Result</returns>
        public VoidPaymentResult Void(VoidPaymentRequest voidPaymentRequest)
        {
            var result = new VoidPaymentResult();
            result.AddError("Void method not supported");
            return result;
        }

        /// <summary>
        /// Process recurring payment
        /// </summary>
        /// <param name="processPaymentRequest">Payment info required for an order processing</param>
        /// <returns>Process payment result</returns>
        public ProcessPaymentResult ProcessRecurringPayment(ProcessPaymentRequest processPaymentRequest)
        {
            var result = new ProcessPaymentResult();
            result.AddError("Recurring payment not supported");
            return result;
        }

        /// <summary>
        /// Cancels a recurring payment
        /// </summary>
        /// <param name="cancelPaymentRequest">Request</param>
        /// <returns>Result</returns>
        public CancelRecurringPaymentResult CancelRecurringPayment(CancelRecurringPaymentRequest cancelPaymentRequest)
        {
            var result = new CancelRecurringPaymentResult();
            result.AddError("Recurring payment not supported");
            return result;
        }

        /// <summary>
        /// Gets a value indicating whether customers can complete a payment after order is placed but not completed (for redirection payment methods)
        /// </summary>
        /// <param name="order">Order</param>
        /// <returns>Result</returns>
        public bool CanRePostProcessPayment(Order order)
        {
            if (order == null)
                throw new ArgumentNullException("order");

            //PayPal Standard is the redirection payment method
            //It also validates whether order is also paid (after redirection) so customers will not be able to pay twice
            
            //payment status should be Pending
            if (order.PaymentStatus != PaymentStatus.Pending)
                return false;

            //let's ensure that at least 1 minute passed after order is placed
            if ((DateTime.UtcNow - order.CreatedOnUtc).TotalMinutes < 1)
                return false;

            return true;
        }

        /// <summary>
        /// Gets a route for provider configuration
        /// </summary>
        /// <param name="actionName">Action name</param>
        /// <param name="controllerName">Controller name</param>
        /// <param name="routeValues">Route values</param>
        public void GetConfigurationRoute(out string actionName, out string controllerName, out RouteValueDictionary routeValues)
        {
            actionName = "Configure";
            controllerName = "PaymentPagseguro";
            routeValues = new RouteValueDictionary() { { "Namespaces", "Nop.Plugin.Payments.Pagseguro.Controllers" }, { "area", null } };
        }

        /// <summary>
        /// Gets a route for payment info
        /// </summary>
        /// <param name="actionName">Action name</param>
        /// <param name="controllerName">Controller name</param>
        /// <param name="routeValues">Route values</param>
        public void GetPaymentInfoRoute(out string actionName, out string controllerName, out RouteValueDictionary routeValues)
        {
            actionName = "PaymentInfo";
            controllerName = "PaymentPagseguro";
            routeValues = new RouteValueDictionary() { { "Namespaces", "Nop.Plugin.Payments.Pagseguro.Controllers" }, { "area", null } };
        }

        public Type GetControllerType()
        {
            return typeof(PaymentPagseguroController);
        }

        public override void Install()
        {
            //settings
            var settings = new PagseguroPaymentSettings()
            {
                WebSiteUrl = "http://www.yoursite.com.br",
                Email = "email@test.com",
                Token= "Seu token aqui"
            };
            _settingService.SaveSetting(settings);

            //locales
            this.AddOrUpdatePluginLocaleResource("Plugins.Payments.Pagseguro.Fields.WebSiteUrl", "Url do site");
            this.AddOrUpdatePluginLocaleResource("Plugins.Payments.Pagseguro.Fields.WebSiteUrl.Hint", "Exemplo: http://www.site.com");
            this.AddOrUpdatePluginLocaleResource("Plugins.Payments.Pagseguro.Fields.Email", "Email");
            this.AddOrUpdatePluginLocaleResource("Plugins.Payments.Pagseguro.Fields.Email.Hint", "Seu email do pagseguro.");
            this.AddOrUpdatePluginLocaleResource("Plugins.Payments.Pagseguro.Fields.Token", "Token de seguranca");
            this.AddOrUpdatePluginLocaleResource("Plugins.Payments.Pagseguro.Fields.Token.Hint", "Token de seguranca da secao de integracao do pagseguro.");
            this.AddOrUpdatePluginLocaleResource("Plugins.Payments.Pagseguro.Fields.AdditionalFee", "Custo extra");
            this.AddOrUpdatePluginLocaleResource("Plugins.Payments.Pagseguro.Fields.AdditionalFee.Hint", "Entre com um custo extra para incluir nos produtos.");
            this.AddOrUpdatePluginLocaleResource("Plugins.Payments.Pagseguro.Fields.PassProductNamesAndTotals", "Passar todos produtos e totais");
            this.AddOrUpdatePluginLocaleResource("Plugins.Payments.Pagseguro.Fields.PassProductNamesAndTotals.Hint", "Marque esta opcao para enviar cada produto e valor.");
            
            base.Install();
        }
        
        public override void Uninstall()
        {
            //locales
            this.DeletePluginLocaleResource("Plugins.Payments.Pagseguro.Fields.WebSiteUrl");
            this.DeletePluginLocaleResource("Plugins.Payments.Pagseguro.Fields.WebSiteUrl.Hint");
            this.DeletePluginLocaleResource("Plugins.Payments.Pagseguro.Fields.Email");
            this.DeletePluginLocaleResource("Plugins.Payments.Pagseguro.Fields.Email.Hint");
            this.DeletePluginLocaleResource("Plugins.Payments.Pagseguro.Fields.Token");
            this.DeletePluginLocaleResource("Plugins.Payments.Pagseguro.Fields.Token.Hint");
            this.DeletePluginLocaleResource("Plugins.Payments.Pagseguro.Fields.AdditionalFee");
            this.DeletePluginLocaleResource("Plugins.Payments.Pagseguro.Fields.AdditionalFee.Hint");
            this.DeletePluginLocaleResource("Plugins.Payments.Pagseguro.Fields.PassProductNamesAndTotals");
            this.DeletePluginLocaleResource("Plugins.Payments.Pagseguro.Fields.PassProductNamesAndTotals.Hint");
            
            base.Uninstall();
        }

        #endregion

        #region Properies

        /// <summary>
        /// Gets a value indicating whether capture is supported
        /// </summary>
        public bool SupportCapture
        {
            get
            {
                return false;
            }
        }

        /// <summary>
        /// Gets a value indicating whether partial refund is supported
        /// </summary>
        public bool SupportPartiallyRefund
        {
            get
            {
                return false;
            }
        }

        /// <summary>
        /// Gets a value indicating whether refund is supported
        /// </summary>
        public bool SupportRefund
        {
            get
            {
                return false;
            }
        }

        /// <summary>
        /// Gets a value indicating whether void is supported
        /// </summary>
        public bool SupportVoid
        {
            get
            {
                return false;
            }
        }

        /// <summary>
        /// Gets a recurring payment type of payment method
        /// </summary>
        public RecurringPaymentType RecurringPaymentType
        {
            get
            {
                return RecurringPaymentType.NotSupported;
            }
        }

        /// <summary>
        /// Gets a payment method type
        /// </summary>
        public Nop.Services.Payments.PaymentMethodType PaymentMethodType
        {
            get
            {
                return Nop.Services.Payments.PaymentMethodType.Redirection;
            }
        }

        #endregion
    }
}
