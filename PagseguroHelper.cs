using Nop.Core.Domain.Payments;

namespace Nop.Plugin.Payments.Pagseguro
{
    /// <summary>
    /// Represents paypal helper
    /// </summary>
    public static class PagseguroHelper
    {
        public static string ToStr(this Uol.PagSeguro.PagSeguroServiceError error)
        {
            string message = string.Empty;
            switch (error.Code)
            {
                case "11001":
                    message = "Email obrigatorio.";
                    break;
                case "11002":
                    message = "Email invalido";
                    break;
                case "11003":
                    message = "Email invalido";
                    break;
                case "11009":
                    message = "Email invalido";
                    break;
                case "11010":
                    message = "Email invalido";
                    break;
                case "11011":
                    message = "Email invalido";
                    break;
                case "11012":
                    message = "Nome do remetente invalido";
                    break;
                case "11013":
                    message = "Cep do remetente invalido";
                    break;
                case "11014":
                    message = "Telefone do remetente invalido";
                    break;
                case "11017":
                    message = "Cep do destinatario invalido";
                    break;
                case "11018":
                    message = "Endereco do destinatario invalido";
                    break;
                case "11019":
	                message = "Numero do destinatario invalido";
                    break;
                case "11020":
                    message = "Complemento do destinatario invalido";
                    break;
                case "11021":
                    message = "shippingAddressDistrict invalid length";
                    break;      
                case "11022":
                    message = "Cidade do destinatario invalido";
                    break;
                case "11023":
                    message = "Estado do destinatario invalido";
                    break;
                case "11024":
                    message = "Carrinho esta vazio";
                    break;
                default:
                    message = error.Message;
                    break;
                //11025	Item Id is required.
                //11026	Item quantity is required.
                //11027	Item quantity out of range: {0}
                //11028	Item amount is required. (e.g. "12.00")
                //11029	Item amount invalid pattern: {0}. Must fit the patern: \d+.\d\{2\}
                //11030	Item amount out of range: {0}
                //11031	Item shippingCost invalid pattern: {0}. Must fit the patern: \d+.\d\{2\}
                //11032	Item shippingCost out of range: {0}
                //11033	Item description is required.
                //11034	Item description invalid length: {0}
                //11035	Item weight invalid Value: {0}
                //11036	Extra amount invalid pattern: {0}. Must fit the patern: -?\d+.\d\{2\}
                //11037	Extra amount out of range: {0}
                //11038	Invalid receiver for checkout: {0}, verify receiver's account status.
            }

            return message;
        }

        /// <summary>
        /// Gets a payment status
        /// </summary>
        /// <param name="paymentStatus">Pagseguro payment status</param>
        /// <param name="pendingReason">Pagseguro pending reason</param>
        /// <returns>Payment status</returns>
        public static PaymentStatus GetPaymentStatus(string paymentStatus)
        {
            var result = PaymentStatus.Pending;

            if (paymentStatus == null)
                paymentStatus = string.Empty;

            switch (paymentStatus.ToLowerInvariant())
            {
                case "completo":
                    result = PaymentStatus.Paid;
                    break;
                case "aguardando pagto":
                    break;
                case "aprovado":
                    result = PaymentStatus.Authorized;
                    break;
                case "em analise":
                    break;
                case "cancelado":
                    break;
                default:
                    break;
            }
            return result;
        }
    }
}

