﻿using Nop.Web.Framework;
using Nop.Web.Framework.Mvc;

namespace Nop.Plugin.Payments.Pagseguro.Models
{
    public class ConfigurationModel : BaseNopModel
    {
        [NopResourceDisplayName("Plugins.Payments.Pagseguro.Fields.WebSiteUrl")]
        public string WebSiteUrl { get; set; }

        [NopResourceDisplayName("Plugins.Payments.Pagseguro.Fields.Email")]
        public string Email { get; set; }

        [NopResourceDisplayName("Plugins.Payments.Pagseguro.Fields.Token")]
        public string Token { get; set; }

        [NopResourceDisplayName("Plugins.Payments.Pagseguro.Fields.AdditionalFee")]
        public decimal AdditionalFee { get; set; }

        [NopResourceDisplayName("Plugins.Payments.Pagseguro.Fields.PassProductNamesAndTotals")]
        public bool PassProductNamesAndTotals { get; set; }
    }
}