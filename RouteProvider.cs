﻿using System.Web.Mvc;
using System.Web.Routing;
using Nop.Web.Framework.Mvc.Routes;

namespace Nop.Plugin.Payments.Pagseguro
{
    public partial class RouteProvider : IRouteProvider
    {
        public void RegisterRoutes(RouteCollection routes)
        {
            routes.MapRoute("Plugin.Payments.Pagseguro.Configure",
                 "Plugins/PaymentPagseguro/Configure",
                 new { controller = "PaymentPagseguro", action = "Configure" },
                 new[] { "Nop.Plugin.Payments.Pagseguro.Controllers" }
            );

            routes.MapRoute("Plugin.Payments.Pagseguro.PaymentInfo",
                 "Plugins/PaymentPagseguro/PaymentInfo",
                 new { controller = "PaymentPagseguro", action = "PaymentInfo" },
                 new[] { "Nop.Plugin.Payments.Pagseguro.Controllers" }
            );
        }
        public int Priority
        {
            get
            {
                return 0;
            }
        }
    }
}
