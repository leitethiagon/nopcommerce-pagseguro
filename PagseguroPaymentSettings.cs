using Nop.Core.Configuration;

namespace Nop.Plugin.Payments.Pagseguro
{
    public class PagseguroPaymentSettings : ISettings
    {
        public string WebSiteUrl { get; set; }
        public string Email { get; set; }
        public string Token { get; set; }
        public decimal AdditionalFee { get; set; }
        public bool PassProductNamesAndTotals { get; set; }
    }
}
