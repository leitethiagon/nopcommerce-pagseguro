﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;
using System.Web.Mvc;
using Nop.Core;
using Nop.Core.Domain.Orders;
using Nop.Core.Domain.Payments;
using Nop.Plugin.Payments.Pagseguro.Models;
using Nop.Services.Configuration;
using Nop.Services.Logging;
using Nop.Services.Orders;
using Nop.Services.Payments;
using Nop.Web.Framework.Controllers;

namespace Nop.Plugin.Payments.Pagseguro.Controllers
{
    public class PaymentPagseguroController : BaseNopPaymentController
    {
        private readonly ISettingService _settingService;
        private readonly IPaymentService _paymentService;
        private readonly IOrderService _orderService;
        private readonly IOrderProcessingService _orderProcessingService;
        private readonly ILogger _logger;
        private readonly IWebHelper _webHelper;
        private readonly PagseguroPaymentSettings _pagseguroPaymentSettings;
        private readonly PaymentSettings _paymentSettings;

        public PaymentPagseguroController(ISettingService settingService, 
            IPaymentService paymentService, IOrderService orderService, 
            IOrderProcessingService orderProcessingService, 
            ILogger logger, IWebHelper webHelper,
            PagseguroPaymentSettings pagseguroPaymentSettings,
            PaymentSettings paymentSettings)
        {
            this._settingService = settingService;
            this._paymentService = paymentService;
            this._orderService = orderService;
            this._orderProcessingService = orderProcessingService;
            this._logger = logger;
            this._webHelper = webHelper;
            this._pagseguroPaymentSettings = pagseguroPaymentSettings;
            this._paymentSettings = paymentSettings;
        }
        
        [AdminAuthorize]
        [ChildActionOnly]
        public ActionResult Configure()
        {
            var model = new ConfigurationModel();
            model.WebSiteUrl = _pagseguroPaymentSettings.WebSiteUrl;
            model.Email = _pagseguroPaymentSettings.Email;
            model.Token = _pagseguroPaymentSettings.Token;
            model.AdditionalFee = _pagseguroPaymentSettings.AdditionalFee;
            model.PassProductNamesAndTotals = _pagseguroPaymentSettings.PassProductNamesAndTotals;

            return View("Nop.Plugin.Payments.Pagseguro.Views.PaymentPagseguro.Configure", model);
        }

        [HttpPost]
        [AdminAuthorize]
        [ChildActionOnly]
        public ActionResult Configure(ConfigurationModel model)
        {
            if (!ModelState.IsValid)
                return Configure();

            //save settings
            _pagseguroPaymentSettings.WebSiteUrl = model.WebSiteUrl;
            _pagseguroPaymentSettings.Email = model.Email;
            _pagseguroPaymentSettings.Token = model.Token;
            _pagseguroPaymentSettings.AdditionalFee = model.AdditionalFee;
            _pagseguroPaymentSettings.PassProductNamesAndTotals = model.PassProductNamesAndTotals;
            _settingService.SaveSetting(_pagseguroPaymentSettings);

            return View("Nop.Plugin.Payments.Pagseguro.Views.PaymentPagseguro.Configure", model);
        }

        [ChildActionOnly]
        public ActionResult PaymentInfo()
        {
            return View("Nop.Plugin.Payments.Pagseguro.Views.PaymentPagseguro.PaymentInfo");
        }

        [NonAction]
        public override IList<string> ValidatePaymentForm(FormCollection form)
        {
            var warnings = new List<string>();
            return warnings;
        }

        [NonAction]
        public override ProcessPaymentRequest GetPaymentInfo(FormCollection form)
        {
            var paymentInfo = new ProcessPaymentRequest();
            return paymentInfo;
        }

        [ValidateInput(false)]
        public ActionResult ReturnPayment(FormCollection form)
        {
            string referencia = form["Referencia"];
            string valorFrete = form["ValorFrete"];
            string status = form["StatusTransacao"];
            string tipoPagamento = form["TipoPagamento"];

            Order order = _orderService.GetOrderByGuid(new Guid(referencia));
            order.PaymentStatus = PagseguroHelper.GetPaymentStatus(status);
            order.PaidDateUtc = DateTime.Now;

            return Content(" ");
        }

        public ActionResult CancelOrder(FormCollection form)
        {
            return RedirectToAction("Index", "Home", new { area = "" });
        }
    }
}